﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoPoolUsed : MonoBehaviour {

    public PoolGenerator _poolGenerator;

    int i = 0;
    void Update () {
        if (Input.GetKey(KeyCode.RightArrow)) {
           GameObject obj = _poolGenerator.GetNextAvailable();
            obj.SetActive(true);
            obj.name = ""+i++;
            obj.transform.position = this.transform.position;

        }
	}
}
